#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# ----------------------------------------------------------
# Wi-Fi

ifeq ($(BOARD_WIFI_VENDOR),ap6212)
WPA_SUPPLICANT_VERSION      := VER_0_8_X
BOARD_WLAN_DEVICE           := bcmdhd
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_$(BOARD_WLAN_DEVICE)
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_$(BOARD_WLAN_DEVICE)

WIFI_DRIVER_MODULE_NAME     := "bcmdhd"
WIFI_DRIVER_MODULE_PATH     := "/system/lib/modules/bcmdhd.ko"
WIFI_DRIVER_FW_PATH_PARAM   := "/sys/module/bcmdhd/parameters/firmware_path"
WIFI_DRIVER_FW_PATH_STA     := "/vendor/firmware/fw_bcm43438a0.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/vendor/firmware/fw_bcm43438a0_p2p.bin"
WIFI_DRIVER_FW_PATH_AP      := "/vendor/firmware/fw_bcm43438a0_apsta.bin"
endif

ifeq ($(BOARD_WIFI_VENDOR),realtek)
WPA_SUPPLICANT_VERSION      := VER_0_8_X
BOARD_WLAN_DEVICE           := rtl8188eu
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_rtl
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_rtl

WIFI_DRIVER_MODULE_NAME     := "wlan"
WIFI_DRIVER_MODULE_PATH     := "/system/vendor/realtek/wlan.ko"
WIFI_DRIVER_MODULE_ARG      := "ifname=wlan0 if2name=p2p0"
WIFI_FIRMWARE_LOADER        := "rtw"
WIFI_DRIVER_FW_PATH_STA     := "STA"
WIFI_DRIVER_FW_PATH_AP      := "AP"
WIFI_DRIVER_FW_PATH_P2P     := "P2P"
WIFI_DRIVER_FW_PATH_PARAM   := "/dev/null"
endif

# ----------------------------------------------------------
# Bluetooth

ifeq ($(BOARD_BLUETOOTH_VENDOR),broadcom)
BOARD_HAVE_BLUETOOTH        := true
BOARD_HAVE_BLUETOOTH_BCM    := true
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := device/friendlyelec/$(TARGET_DEVICE)/bluetooth
endif

